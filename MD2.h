#ifndef _MD2_H
#define _MD2_H

// MD2.h by PhilVaz (based on GameTutorials.com)
// MD2 Model Head and Data Definitions
// The MD2 Model definition and Quake 2 Game are owned by Id Software

// Oct 30, 2005

// HEADER FILE ///////////////////////////////////////////////

#include <windows.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string>
#include <fstream>
#include <vector>

#include <gl\gl.h>
#include <gl\glu.h>
#include <gl\glaux.h>

using namespace std;

#define MD2_MAX_TRIS            4096    // standard MD2 limits
#define MD2_MAX_VERTS           2048
#define MD2_MAX_TEXVERTS        2048
#define MD2_MAX_FRAMES          512
#define MD2_MAX_SKINS           32
#define MD2_MAX_FRAMESIZE       (MD2_MAX_VERTS * 4 + 128)

#define MAX_TEXTURES            100

#define ANIM_KEY                5.0f   // animation speed between key frames


class CVector3  // 3D point class to save model vertices
{
   public: float x, y, z;
};

class CVector2  // 2D point class for UV texture coords
{
   public: float x, y;
};

struct tFace  // triangle face struct
{
   int vertIndex[3];  // indices for triangle verts
   int texIndex[3]; // indices for texture coords
};

struct tMaterialInfo   // material information of texture map
{
   char name[255];      // texture name
   char filename[255];  // texture file name
   BYTE color[3];       // object color RGB
   int texID;           // texture ID
   float uTile;         // U tiling texture
   float vTile;         // V tiling texture
   float uOffset;       // U offset texture
   float vOffset;       // V offset texture
} ;

struct t3DObject  // information for model or scene (could be a class with functions)
{
   int numVerts;        // number of model vertices
   int numFaces;        // number of model faces (triangles)
   int numTexVerts;     // number of texture coords
   int matID;           // material ID, index to texture array
   bool bHasTex;        // TRUE if texture map for this object
   char name[255];      // name of object
   CVector3 *pVerts;    // object vertices
   CVector3 *pNormals;  // object normals (for lighting)
   CVector2 *pTexVerts; // texture UV coords
   tFace *pFaces;       // face information
};

struct tAnimationInfo  // animation information
{
   char name[255];     // name of animation (idle, jump, pain, etc)
   int startFrame;     // start frame number for this animation
   int endFrame;       // end frame number for this animation
};

struct t3DModel  // model structure
{
   int numFrames;                     // number of frames in model
   int numMats;                       // number of materials for model
   int numAnims;                      // number of animations in model
   int currentAnim;                   // current index into pAnims list
   int currentFrame;                  // current frame of current animation
   vector<t3DObject> pObject;         // object list for model
   vector<tMaterialInfo> pMats;       // list of material info (textures and colors)
   vector<tAnimationInfo> pAnims;     // list of animations
};


struct tMD2Header              // MD2 Model Header Information
{
   int magic;                  // identifier (always IDP2)
   int version;                // version number (always 8)
   int skinWidth;              // skin width in pixels
   int skinHeight;             // skin height in pixels
   int frameSize;              // size in bytes of frames
   int numSkins;               // number of model skins
   int numVerts;               // number of vertices (same for each frame)
   int numTexVerts;            // number of texture coords
   int numTris;                // number of faces (triangle polygons)
   int numGLCommands;          // number of OpenGL commands for rendering
   int numFrames;              // number of animation frames
   int offsetSkins;            // offset for skin data
   int offsetTexVerts;         // offset for texture data
   int offsetTris;             // offset for face data
   int offsetFrames;           // offset for frames data
   int offsetGLCommands;       // offset for OpenGL commands data
   int offsetEnd;              // offset to end of file
};

struct tMD2AliasTriangle  // vertices for current frame
{
   BYTE vertex[3];
   BYTE lightNormalIndex;
};

struct tMD2Triangle  // vertices and normals for frames
{
   float vertex[3];
   float normal[3];
};

struct tMD2Face  // indices into vertex and texture coord arrays
{
   short vertIndex[3];
   short texIndex[3];
};

struct tMD2TexVert  // UV coords for texture mapping
{
   short u, v;
};

struct tMD2AliasFrame  // animation scale, translation, name info, verts
{
   float scale[3];
   float translate[3];
   char name[16];
   tMD2AliasTriangle aliasVerts[1];
};

struct tMD2Frame  // frame verts after transform
{
   char name[16];
   tMD2Triangle *pVerts;
};

typedef char tMD2Skin[64];  // skin name

class CLoadMD2  // model loading class
{

   public:  // init and loader

   CLoadMD2();  // init data members and import MD2 file
   bool ImportMD2(t3DModel *pModel, char *filename, char *texname);

   private:  // functions and member vars

   void ReadMD2Data();                            // read and save MD2 data
   void ParseAnimations(t3DModel *pModel);        // parse and calculate anims
   void ConvertDataStructures(t3DModel *pModel);  // convert member vars
   void CleanUp();                                // free memory and close

   FILE *FilePointer;                      // file pointer

   tMD2Header               Header;        // MD2 header data
   tMD2Skin                 *pSkins;       // skin data
   tMD2TexVert              *pTexVerts;    // texture coords
   tMD2Face                 *pTris;        // face index info
   tMD2Frame                *pFrames;      // animation frames
};

// END MD2.h

#endif