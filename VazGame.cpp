// VazGame.cpp by PhilVaz
// VazEngine general game functions GameInit, GameMain, GameQuit
// Nov 20, 2005

#include "VazGame.h"

// globals

bool draw_ok;   // whether OpenGL init ok
bool sound_ok;  // whether DirectSound init ok
bool input_ok;  // whether DirectInput init ok

int game_state  = GAME_STATE_DEMO_INIT;  // initial game state, level, total score

extern PLAYER_STRUCT Player;
extern PARTICLE_STRUCT Particles[MAX_PARTICLES];

extern int bullet_wait;

extern t3DModel model_player;

extern UCHAR key_data[256];

// functions

///////////////////////////////////////////////////////////
// GAME INITIALIZATION
///////////////////////////////////////////////////////////

void GameInit()
{
   int i;

   srand(GetTickCount()); // seed the random numbers
   
   draw_ok = DrawInit();
                  
   sound_ok = SoundInit();  // Initialize DirectSound

   input_ok = InputInit();  // Initialize DirectInput
       
   CreateWorld();                       // create world objects
     
   Player.y = 3.5f;  // constant player Y height position
   
   bullet_wait = BULLET_PAUSE;  // reset bullet wait time
         
   // reset particles (only needed once during init)
   for (i = 0; i < MAX_PARTICLES; i++) Particles[i].alive = FALSE;
            
   ShowCursor(FALSE);
   
} // END OF GameInit


///////////////////////////////////////////////////////////
// GAME MAIN LOOP AND PROCESSING
///////////////////////////////////////////////////////////

void GameMain()
{

   if (!draw_ok) return; // make sure OpenGL and textures loaded properly

   switch(game_state)
   {
      case GAME_STATE_DEMO_INIT:
      {

         SetPlayer();
         
         SetEnemy();
         
         SetObjects();
         
         game_state = GAME_STATE_DEMO_RUN;

      } break;

      case GAME_STATE_DEMO_RUN:
      {

         MoveEnemy();
         
         MoveParticles();

         RenderScene();
         
         // if space key pressed, set state to GAME_INIT
         if (KEYDOWN(key_data, DIK_SPACE))
         {
            game_state = GAME_STATE_GAME_INIT;
         }

      } break;

      case GAME_STATE_GAME_INIT:
      {

         // reset level to initial values
                           
         SetPlayer();
         
         SetEnemy();
         
         SetObjects();

         game_state = GAME_STATE_GAME_RUN;

      } break;

      case GAME_STATE_GAME_RUN:
      {

         MoveBullets();
         
         MoveEnemy();
         
         MoveParticles();

         CheckCollisions();

         RenderScene();

         // if 'p' key pressed, set state to GAME_PAUSE
         if (KEYDOWN(key_data, DIK_P))
         {
            game_state = GAME_STATE_GAME_PAUSE;
         }

      } break;

      case GAME_STATE_GAME_RESET:
      {

         SetEnemy();
                  
         game_state = GAME_STATE_GAME_RUN;

      } break;

      case GAME_STATE_GAME_PAUSE:
      {

         // if <SPACE BAR> key pressed resume game
         if (KEYDOWN(key_data, DIK_SPACE)) game_state = GAME_STATE_GAME_RUN;

      } break;

   } // end switch

} // END OF GameMain


///////////////////////////////////////////////////////////
// GAME QUIT AND CLEAN UP
///////////////////////////////////////////////////////////

void GameQuit()
{

   int i; // for loop
   
   DrawQuit();
         
   SoundQuit();  // turn off DirectSound and release buffers

   InputQuit();  // turn off DirectInput and release keyboard, mouse, etc
      
   for (i = 0; i < model_player.numFrames; i++)  // free faces, normals, vertices, and texture coords
   {
      if (model_player.pObject[i].pFaces)    delete [] model_player.pObject[i].pFaces;
      if (model_player.pObject[i].pNormals)  delete [] model_player.pObject[i].pNormals;
      if (model_player.pObject[i].pVerts)    delete [] model_player.pObject[i].pVerts;
      if (model_player.pObject[i].pTexVerts) delete [] model_player.pObject[i].pTexVerts;
   }

} // END OF GameQuit