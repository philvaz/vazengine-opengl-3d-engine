#ifndef _VAZDEMO_H
#define _VAZDEMO_H

// VazDemo.h by PhilVaz
// Vaz Demo game specifics
// Nov  20, 2005  VazDemo with new models, sounds, 3D engine

// HEADER FILE ///////////////////////////////////////////////

#define WIN32_LEAN_AND_MEAN

#include <windows.h>
#include <windowsx.h>
#include <mmsystem.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#include <gl\gl.h>     // OpenGL32 library
#include <gl\glu.h>    // GLU32    library
#include <gl\glaux.h>  // GLAUX    library

#include "Vaz2D.h"
#include "Vaz3D.h"
#include "VazGame.h"
#include "VazInput.h"
#include "VazModel.h"
#include "VazPart.h"
#include "VazSound.h"
#include "WIN32.h"

#include "MD2.h"
//#include "VazDemo.h"

// GAME CONSTANTS

// TEXTURES

#define TEX_FLOOR        0
#define TEX_SKY          1
#define TEX_WALL         2
#define TEX_CAMO         3

// ANIMATIONS

#define ANIM_IDLE_START        0    // MD2 model animations
#define ANIM_IDLE_END         39
#define ANIM_RUN_START        40
#define ANIM_RUN_END          45
#define ANIM_CRAWL_START     154
#define ANIM_CRAWL_END       159
#define ANIM_DIE1_START      173
#define ANIM_DIE1_END        177
#define ANIM_DIE2_START      178
#define ANIM_DIE2_END        183
#define ANIM_DIE3_START      184
#define ANIM_DIE3_END        189
#define ANIM_DIE4_START      190
#define ANIM_DIE4_END        197

#define TYPE_CAMO        1
#define TYPE_AMMO        2
#define TYPE_FUEL        3

#define MAX_TEXS           4  // max textures
#define MAX_LISTS          4  // max display lists
#define MAX_OBJECTS      100  // max objects (camo and fuel and ammo)
#define MAX_BULLETS       25  // max player bullets
#define MAX_PARTICLES   2500  // max explosion particles

#define COLOR_BLK          0  // black particle explosion colors
#define COLOR_WHT          1  // white
#define COLOR_RED          2  // red
#define COLOR_TAN          3  // tan (wood)
#define COLOR_MAR          4  // maroon (dark red)
#define COLOR_DGR          5  // dark green
#define COLOR_BRN          6  // brown
#define COLOR_YEL          7  // yellow

#define EXPLODE_WALL       1
#define EXPLODE_CAMO       2
#define EXPLODE_GROUND     3

#define PLAYER_SPEED_INC     .005f  // increment player speed
#define PLAYER_SPEED_MAX    2.5f    // max speed
#define PLAYER_SPEED_MIN     .1f    // min speed
#define ROTATE_SPEED         .2f    // rotation speed
#define BULLET_SPEED        5.0f    // player and enemy bullet speed
#define BULLET_DURATION   200       // bullet duration in frames
#define GRAVITY_SPEED       0.1f    // gravity for particle effects

#define BULLET_PAUSE          15  // pause time between bullets

// SOUNDS

#define SOUND_EXPLODE0    101
#define SOUND_EXPLODE1    102
#define SOUND_EXPLODE2    103
#define SOUND_EXPLODE3    104
#define SOUND_EXPLODE4    105
#define SOUND_EXPLODE5    106
#define SOUND_EXPLODE6    107
#define SOUND_EXPLODE7    108

#define SOUND_BLASTER1    109
#define SOUND_BLASTER2    110
#define SOUND_BLASTER3    111
#define SOUND_BLASTER4    112
#define SOUND_BLASTER5    113


// STRUCTS

typedef struct  // Struct for player and weapon position/view
{
   GLfloat x;
   GLfloat y;
   GLfloat z;
   GLfloat Look;
   double  Angle;
} PLAYER_STRUCT;

typedef struct  // Struct for bullets
{
   bool alive;
   GLfloat x;
   GLfloat y;
   GLfloat z;
   GLfloat dx;
   GLfloat dy;
   GLfloat dz;
   int count;
} BULLET_STRUCT;

typedef struct  // Struct for Object boxes
{
   bool alive;
   GLfloat x;
   GLfloat y;
   GLfloat z;
   int type;
} OBJECT_STRUCT;

typedef struct    // Struct for Particles
{
   bool alive;
   GLfloat x;
   GLfloat y;
   GLfloat z;
   GLfloat dx;
   GLfloat dy;
   GLfloat dz;
   int color;
   int count;
} PARTICLE_STRUCT;

// FUNCTIONS

void SetPlayer();
void SetEnemy();
void SetObjects();  // set camo boxes

void MoveEnemy();
void MoveBullets();

void FireBullet();

void CheckCollisions();

#endif