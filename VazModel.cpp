// VazModel.cpp by PhilVaz
// VazEngine model animate and render functions
// Nov 20, 2005

#include "VazModel.h"

// globals

CLoadMD2 model_loader;  // model loader class

t3DModel model_player;  // player model

UINT texture_player[MAX_TEXTURES] = {0};  // model texture info referenced by an ID

extern HWND game_window;


//////////////////////////////////////////////////
// Texture an MD2 Model
//////////////////////////////////////////////////

bool TextureModel(UINT texArray[], LPSTR filename, int texID)
{

   AUX_RGBImageRec *pBitmap = NULL;

   if (!filename) return FALSE;  // check if BMP file exists

   pBitmap = auxDIBImageLoad(filename);  // load BMP file
   if (!pBitmap) return FALSE;  // check if BMP loaded properly

   glGenTextures(1, &texArray[texID]);  // generate texture with ID in array
   glPixelStorei (GL_UNPACK_ALIGNMENT, 1);  // set align for each pixel row
   glBindTexture(GL_TEXTURE_2D, texArray[texID]);  // bind and init texture and build mipmaps
   gluBuild2DMipmaps(GL_TEXTURE_2D, 3, pBitmap->sizeX, pBitmap->sizeY, GL_RGB, GL_UNSIGNED_BYTE, pBitmap->data);

   glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR_MIPMAP_NEAREST);  // texture map quality
   glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR_MIPMAP_LINEAR);

   if (pBitmap)  // free BMP
   {
      if (pBitmap->data) free(pBitmap->data);  // free data
      free(pBitmap);  // free pointer
   }

   return TRUE;  // BMP loaded properly

} // end TextureModel


//////////////////////////////////////////////////
// Render and Animate MD2 Model
//////////////////////////////////////////////////

void RenderModel(t3DModel *pModel, int r)
{
   int i, j;  // for loops

   if (pModel->pObject.size() <= 0) return;  // do we have valid object?

   tAnimationInfo *pAnim = &(pModel->pAnims[pModel->currentAnim]);  // get current animation

   int nextFrame = (pModel->currentFrame + 1) % pAnim->endFrame;  // get current frame

   if (nextFrame == 0) nextFrame = pAnim->startFrame;  // if zero start animation over

   t3DObject *pFrame      = &pModel->pObject[pModel->currentFrame];  // get current keyframe
   t3DObject *pNextFrame  = &pModel->pObject[nextFrame];             // get next for interpolation
   t3DObject *pFirstFrame = &pModel->pObject[0];  // get first keyframe for addr to texture and face info

   float t = ReturnTime(pModel, nextFrame);  // get interpolation time (from 0 to 1 = 100%)

   glEnable(GL_TEXTURE_2D);   // enable texturing
   
   if (r == 0) glBindTexture(GL_TEXTURE_2D, texture_player[0]); // use player texture
            
   glBegin(GL_TRIANGLES);  // start rendering model (based on render mode)

   for (j = 0; j < pFrame->numFaces; j++)  // loop through all faces of current frame
   {
      for (i = 0; i < 3; i++)  // loop each vertex of triangle
      {
         int vertIndex = pFirstFrame->pFaces[j].vertIndex[i];   // get index for each face point
         int texIndex  = pFirstFrame->pFaces[j].texIndex[i];  // get index for each texture coord

         if (pFirstFrame->pTexVerts)  // check UVW map applied to object
         {
            // get texture coordinate for this vertex
            glTexCoord2f(pFirstFrame->pTexVerts[texIndex].x, pFirstFrame->pTexVerts[texIndex].y);
         }
         // store current and next frame vertex
         CVector3 vPoint1 = pFrame->pVerts[vertIndex];  // for interpolation equation below
         CVector3 vPoint2 = pNextFrame->pVerts[vertIndex];  // save current and next frame vertex

         // using equation p(t) = p0 + t(p1 - p0) with time t, find interpolated x, y, z
         glVertex3f(vPoint1.x + t * (vPoint2.x - vPoint1.x),
                                     vPoint1.y + t * (vPoint2.y - vPoint1.y),
                                     vPoint1.z + t * (vPoint2.z - vPoint1.z));

      } // end for i

   } // end for j

   glEnd();  // stop rendering
   
   glDisable(GL_TEXTURE_2D);   // disable texturing

   
} // end RenderModel


/////////////////////////////////////////////////////
// Return current interpolation time for animations
/////////////////////////////////////////////////////

float ReturnTime(t3DModel *pModel, int nextFrame)
{

   static float elapsed_time   = 0.0f;
   static float last_time	  = 0.0f;

   float time = GetTickCount();    // get current time in milliseconds
   elapsed_time = time - last_time;  // get time elapsed since last

   float t = elapsed_time / (1000.0f / ANIM_KEY);  // t = elapsed divided by 1 second = t

   if (elapsed_time >= (1000.0f / ANIM_KEY))  // check if advance to next keyframe
   {
      pModel->currentFrame = nextFrame;  // set current to next keyframe
      last_time = time;  // save last time
   }

   return t;  // t is interpolation time

} // end ReturnTime


//////////////////////////////////////////////////
// Load all MD2 Model files
//////////////////////////////////////////////////

bool LoadModels()
{
   int i;  // for loop

   if (!model_loader.ImportMD2(&model_player, "player.md2", "player.bmp"))
   {
      MessageBox(game_window, "Sorry, player.md2 model did not load properly or not a valid MD2 model","Load MD2 Model Error",MB_OK);
      return FALSE;
   }

   // texture player model
   for (i = 0; i < model_player.numMats; i++)  // loop through materials
   {
      if (strlen(model_player.pMats[i].filename) > 0)  // is there a file name for material?
      {
         if (!TextureModel(texture_player, model_player.pMats[i].filename, i))
         {
            MessageBox(game_window, "Sorry, player.bmp skin did not load properly or not a valid BMP","Load MD2 Model Error",MB_OK);
            return FALSE;
         } // end if
      } // end if
      model_player.pMats[i].texID = i;  // set texture ID for material
   } // end for i




   return TRUE;  // success loading MD2 models and textures

} // end LoadModels

