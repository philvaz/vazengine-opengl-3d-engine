// Vaz3D.cpp by PhilVaz
// VazEngine 3D graphics render functions
// Nov 20, 2005

#include "Vaz3D.h"

// globals

int width  = WINDOW_WIDTH;
int height = WINDOW_HEIGHT;

HDC   game_dc = NULL;            // global device context (GDI) handle
HGLRC game_rc = NULL;            // global rendering context (OpenGL) handle

double PI = 3.14159265359;       // define PI constant

GLuint texture[MAX_TEXS];        // storage for textures

GLuint RoomList;    // storage for room display list
GLuint Sky1List;    // storage for sky 1 list
GLuint Sky2List;    // storage for sky 2 list
GLuint CamoList;    // storage for camo box list

GLUquadricObj *game_sphere = NULL;  // pointer for bullet spheres

GLfloat sky_degrees = 0.0f;  // sky rotate degrees

int FPS = 0;              // running frames per second
int curr_FPS = 0;      // current calculated FPS
float prev_time = 0.0f;   // time elapsed from previous frame
float curr_time;          // new current time

extern HWND game_window;

extern PLAYER_STRUCT Player;
extern BULLET_STRUCT Bullets[MAX_BULLETS];
extern OBJECT_STRUCT Objects[MAX_OBJECTS];
extern PARTICLE_STRUCT Particles[MAX_PARTICLES];

extern t3DModel model_player;

extern GLuint font_base;

extern char text[80];

// functions

//////////////////////////////////////////
// Init OpenGL and Drawing
//////////////////////////////////////////

bool DrawInit()
{
   int pf;               // pixel format
   DEVMODE game_screen;  // full screen mode
   HFONT game_font;      // for game font chars

   // temporary change to full screen mode
   
   ZeroMemory(&game_screen, sizeof(game_screen)); // clear out size of DEVMODE struct

   game_screen.dmSize = sizeof(game_screen);
   game_screen.dmPelsWidth  = width;
   game_screen.dmPelsHeight = height;
   game_screen.dmBitsPerPel = WINDOW_BPP;
   game_screen.dmFields = DM_PELSWIDTH | DM_PELSHEIGHT | DM_BITSPERPEL;

   ChangeDisplaySettings(&game_screen, CDS_FULLSCREEN);

   game_dc = GetDC(game_window); // get the GDI device context

   // set up the pixel format desc struct

   PIXELFORMATDESCRIPTOR pfd =
   {
      sizeof(PIXELFORMATDESCRIPTOR),   // size of this PFD
      1,                               // version number
      PFD_DRAW_TO_WINDOW |             // supports window
      PFD_SUPPORT_OPENGL |             // supports OpenGL
      PFD_DOUBLEBUFFER,                // support double buff
      PFD_TYPE_RGBA,                   // request RGBA format
      WINDOW_BPP,                      // select color depth
      0, 0, 0, 0, 0, 0,                // color bits ignored
      0,                               // no alpha buff
      0,                               // shift bit ignored
      0,                               // no accum buff
      0, 0, 0, 0,                      // accum bits ignored
      16,                              // 16-bit Z-buff (depth buff)
      0,                               // no stencil buff
      0,                               // no aux buff
      PFD_MAIN_PLANE,                  // main drawing layer
      0,                               // reserved
      0, 0, 0                          // layer masks ignored
   };

   if (!(pf = ChoosePixelFormat(game_dc, &pfd)))  // match the pixel format
   {
      MessageBox(game_window, "OpenGL could not be initialized -- ChoosePixelFormat Error","OpenGL Error",MB_OK);
      return FALSE; // error returned
   }

   if (!SetPixelFormat(game_dc, pf, &pfd))        // set the pixel format
   {
      MessageBox(game_window, "OpenGL could not be initialized -- SetPixelFormat Error","OpenGL Error",MB_OK);
      return FALSE; // error returned
   }

   if (!(game_rc = wglCreateContext(game_dc)))    // create the rendering context
   {
      MessageBox(game_window, "OpenGL could not be initialized -- CreateContext Error","OpenGL Error",MB_OK);
      return FALSE; // error returned
   }

   if (!wglMakeCurrent(game_dc, game_rc))         // make it current
   {
      MessageBox(game_window, "OpenGL could not be initialized -- MakeCurrent Error","OpenGL Error",MB_OK);
      return FALSE; // error returned
   }

   // OpenGL Initialized Okay

   // set focus and set up viewport 3D perspective
   
   SetFocus(game_window);
   glViewport(0, 0, width, height);
   glMatrixMode(GL_PROJECTION);
   glLoadIdentity();
   gluPerspective(45.0f, (GLfloat)width / (GLfloat)height, 0.5f, 5000.0f);

   glMatrixMode(GL_MODELVIEW);
   glLoadIdentity();

   //SetCursorPos(width - 2, height / 2);   // set pointer start location
   MoveCamera(-10.0f);                      // move closer to center
   
   // ORDER is important here, font must be loaded before textures to show
   font_base = glGenLists(96); // create storage for 96 chars
   game_font = CreateFont(16, 0, 0, 0, FW_NORMAL, FALSE, FALSE, FALSE, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, ANTIALIASED_QUALITY, FF_DONTCARE | DEFAULT_PITCH, "Courier");
   SelectObject(game_dc, game_font); // select and set the font
   wglUseFontBitmaps(game_dc, 32, 96, font_base);
   
   glEnable(GL_CULL_FACE);      // remove hidden faces
   glEnable(GL_TEXTURE_2D);     // enable 2D texture maps
      
   if (!LoadTextures())  // attempt to load all world textures
   {
      MessageBox(game_window, "Sorry, Textures did not load properly","OpenGL Error",MB_OK);
      return FALSE;
   }
   
   if (!LoadModels())  // attempt to load all MD2 models and textures
   {
      MessageBox(game_window, "Sorry, model was not loaded properly or not a valid MD2 model","OpenGL Error",MB_OK);
      return FALSE;
   }

   glClearColor(0.75f,0.75f,1.0f,0.0f); // clear screen to light blue
   glEnable(GL_DEPTH_TEST);             // enable depth testing
   glClearDepth(1.0);                   // set depth

   game_sphere = gluNewQuadric();  // create storage for bullet spheres

   return TRUE;
   
} // END OF DrawInit


//////////////////////////////////////////
// Quit OpenGL and restore display
//////////////////////////////////////////

void DrawQuit()
{
   if (font_base) glDeleteLists(font_base, 96);    // delete the game font
   
   if (game_sphere) gluDeleteQuadric(game_sphere); // delete bullet sphere
   
   if (game_rc) // check if rendering context (OpenGL) exists
   {
      glDeleteLists(RoomList, MAX_LISTS);  // delete the world call lists
      wglMakeCurrent(NULL,NULL);
      wglDeleteContext(game_rc);           // delete rendering context
   }

   // release the device context (GDI) from the game window
   ReleaseDC(game_window, game_dc);

   // return to original display settings
   ChangeDisplaySettings(NULL,NULL);

} // END OF DrawQuit


/////////////////////////////////////////
// Moves camera in current direction
/////////////////////////////////////////

void MoveCamera(GLdouble steps)
{

   GLdouble xd,zd; // x and z delta

   xd =  steps * cos(Player.Angle);  // moves on X plane in player direction
   zd = -steps * sin(Player.Angle);  // moves on Z plane in player direction

   Player.x += (GLfloat)xd;     // do the move on X
   Player.z += (GLfloat)zd;     // do the move on Z

   if (Player.x >  990.0f) Player.x =  990.0f; // limit on +X
   if (Player.x < -990.0f) Player.x = -990.0f; // limit on -X
   if (Player.z >  990.0f) Player.z =  990.0f; // limit on +Z
   if (Player.z < -990.0f) Player.z = -990.0f; // limit on -Z
         
} // END OF MoveCamera


///////////////////////////////
// Load a BMP Image
///////////////////////////////

AUX_RGBImageRec *LoadBMP(char *Filename)
{

   FILE *File = NULL;            // file handle

   if (!Filename) return NULL;   // make sure there is a file name

   File = fopen(Filename,"r");   // attempt to read the file

   if (File)                     // if file exists, close and return the handle
   {
      fclose(File);
      return auxDIBImageLoad(Filename);
   }

   return NULL;                  // load failed

} // END OF LoadBMP


/////////////////////////////////////////
// Load and convert BMPs to Textures
/////////////////////////////////////////

bool LoadTextures()
{
   bool Status = FALSE;    // status indicator
   int i;

   AUX_RGBImageRec *TextureImage[MAX_TEXS];  // create storage space

   memset(TextureImage,0,sizeof(void *) * MAX_TEXS); // initialize pointer to NULL

   // load BMPs checking for errors
   if ((TextureImage[TEX_FLOOR] = LoadBMP("Floor.bmp")) &&
       (TextureImage[TEX_SKY]   = LoadBMP("Sky.bmp"))   &&
       (TextureImage[TEX_WALL]  = LoadBMP("Wall.bmp"))  &&
       (TextureImage[TEX_CAMO]  = LoadBMP("Camo.bmp")))
   {
      Status = TRUE;  // success in reading in BMPs

      glGenTextures(MAX_TEXS, &texture[0]);  // create the texture

      for (i = 0; i < MAX_TEXS; i++)
      {
         glBindTexture(GL_TEXTURE_2D, texture[i]);
         gluBuild2DMipmaps(GL_TEXTURE_2D, 3, TextureImage[i]->sizeX, TextureImage[i]->sizeY, GL_RGB, GL_UNSIGNED_BYTE, TextureImage[i]->data);
         glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR_MIPMAP_LINEAR);
         glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);

         if (TextureImage[i])  // if texture exists, free image and struct
         {
            if (TextureImage[i]->data) free(TextureImage[i]->data);
            free(TextureImage[i]);
         } // end if

      } // end for

   } // end if LoadBMP

   return Status;

} // END OF LoadTextures


//////////////////////////////////////////////////////////
// Create all world objects and generate display lists
//////////////////////////////////////////////////////////

void CreateWorld()
{

   // Generate display lists (room, 2 sky layers, objects, enemies)
   RoomList = glGenLists(MAX_LISTS);
   Sky1List = RoomList + 1; Sky2List = Sky1List + 1;
   CamoList = Sky2List + 1;
         
   // create the room, 4 walls and a floor
   glNewList(RoomList,GL_COMPILE);
   glEnable(GL_CULL_FACE);      // remove hidden faces
   glEnable(GL_TEXTURE_2D);     // enable texture map

   glBindTexture(GL_TEXTURE_2D, texture[TEX_FLOOR]); // use floor texture

   glBegin(GL_QUADS);
      // floor (clockwise)
      glNormal3f( 0.0f, 1.0f, 0.0f);  // normal points up Y axis
      glTexCoord2f( 0.0f, 0.0f); glVertex3f(-1000.0f,-2.0f, 1000.0f);
      glTexCoord2f(50.0f, 0.0f); glVertex3f( 1000.0f,-2.0f, 1000.0f);
      glTexCoord2f(50.0f,50.0f); glVertex3f( 1000.0f,-2.0f,-1000.0f);
      glTexCoord2f( 0.0f,50.0f); glVertex3f(-1000.0f,-2.0f,-1000.0f);
   glEnd();

   glBindTexture(GL_TEXTURE_2D, texture[TEX_WALL]); // use wall texture

   glBegin(GL_QUADS);

      // front wall (cw)
      glNormal3f( 0.0f, 0.0f, -1.0f);  // normal points into screen
      glTexCoord2f( 0.0f, 0.0f); glVertex3f( 1000.0f,-2.0f, 1000.0f);
      glTexCoord2f(50.0f, 0.0f); glVertex3f(-1000.0f,-2.0f, 1000.0f);
      glTexCoord2f(50.0f, 1.0f); glVertex3f(-1000.0f, 6.0f, 1000.0f);
      glTexCoord2f( 0.0f, 1.0f); glVertex3f( 1000.0f, 6.0f, 1000.0f);

      // back wall (ccw)
      glNormal3f( 0.0f, 0.0f, 1.0f);  // normal points toward viewer
      glTexCoord2f( 0.0f, 0.0f); glVertex3f(-1000.0f,-2.0f,-1000.0f);
      glTexCoord2f(50.0f, 0.0f); glVertex3f( 1000.0f,-2.0f,-1000.0f);
      glTexCoord2f(50.0f, 1.0f); glVertex3f( 1000.0f, 6.0f,-1000.0f);
      glTexCoord2f( 0.0f, 1.0f); glVertex3f(-1000.0f, 6.0f,-1000.0f);

      // right wall (cw)
      glNormal3f(-1.0f, 0.0f, 0.0f);  // normal points left
      glTexCoord2f( 0.0f, 0.0f); glVertex3f( 1000.0f,-2.0f,-1000.0f);
      glTexCoord2f(50.0f, 0.0f); glVertex3f( 1000.0f,-2.0f, 1000.0f);
      glTexCoord2f(50.0f, 1.0f); glVertex3f( 1000.0f, 6.0f, 1000.0f);
      glTexCoord2f( 0.0f, 1.0f); glVertex3f( 1000.0f, 6.0f,-1000.0f);

      // left wall (ccw)
      glNormal3f( 0.0f, 1.0f, 0.0f);  // normal points up
      glTexCoord2f( 0.0f, 0.0f); glVertex3f(-1000.0f,-2.0f, 1000.0f);
      glTexCoord2f(50.0f, 0.0f); glVertex3f(-1000.0f,-2.0f,-1000.0f);
      glTexCoord2f(50.0f, 1.0f); glVertex3f(-1000.0f, 6.0f,-1000.0f);
      glTexCoord2f( 0.0f, 1.0f); glVertex3f(-1000.0f, 6.0f, 1000.0f);

   glEnd();

   glDisable(GL_TEXTURE_2D);   // disable texturing
   glDisable(GL_CULL_FACE);    // disable face culling

   glEndList();

   // create sky layer 1
   glNewList(Sky1List,GL_COMPILE);
   glEnable(GL_BLEND);                        // enable blending
   glEnable(GL_TEXTURE_2D);                   // enable texturing
   glBlendFunc(GL_SRC_ALPHA, GL_SRC_ALPHA);   // set blend mode

   glBindTexture(GL_TEXTURE_2D, texture[TEX_SKY]);  // use sky texture

   glBegin(GL_QUADS);
      glNormal3f(0.0f,-1.0f,0.0f);  // normal points down
      glTexCoord2f(0.0f, 0.0f); glVertex3f( 10000.0f, 120.0f,  10000.0f);
      glTexCoord2f(2.0f, 0.0f); glVertex3f(-10000.0f, 120.0f,  10000.0f);
      glTexCoord2f(2.0f, 2.0f); glVertex3f(-10000.0f, 120.0f, -10000.0f);
      glTexCoord2f(0.0f, 2.0f); glVertex3f( 10000.0f, 120.0f, -10000.0f);
   glEnd();

   glDisable(GL_TEXTURE_2D);  // disable texturing
   glDisable(GL_BLEND);       // disable blending

   glEndList();

   // create sky layer 2, up higher than sky 1
   glNewList(Sky2List,GL_COMPILE);
   glEnable(GL_TEXTURE_2D);   // enable texturing

   glBindTexture(GL_TEXTURE_2D, texture[TEX_SKY]); // use sky texture

   glBegin(GL_QUADS);
      glNormal3f(0.0f,-1.0f,0.0f);   // normal points down
      glTexCoord2f(0.0f, 0.0f); glVertex3f( 10000.0f, 150.0f,  10000.0f);
      glTexCoord2f(3.0f, 0.0f); glVertex3f(-10000.0f, 150.0f,  10000.0f);
      glTexCoord2f(3.0f, 3.0f); glVertex3f(-10000.0f, 150.0f, -10000.0f);
      glTexCoord2f(0.0f, 3.0f); glVertex3f( 10000.0f, 150.0f, -10000.0f);
   glEnd();

   glDisable(GL_TEXTURE_2D);   // disable texturing

   glEndList();

   // draw some 3D ground objects

   // CAMOS

   glNewList(CamoList,GL_COMPILE);

   glEnable(GL_CULL_FACE);      // remove hidden faces
   glEnable(GL_TEXTURE_2D);

   glBindTexture(GL_TEXTURE_2D, texture[TEX_CAMO]); // use camo texture

   glBegin(GL_QUADS);

      glTexCoord2f(0.0f, 0.0f); glVertex3f( 7, 7, 7);   // face 1
      glTexCoord2f(2.0f, 0.0f); glVertex3f(-7, 7, 7);
      glTexCoord2f(2.0f, 2.0f); glVertex3f(-7,-1, 7);
      glTexCoord2f(0.0f, 2.0f); glVertex3f( 7,-1, 7);

      glTexCoord2f(0.0f, 0.0f); glVertex3f( 7,-1,-7);   // face 2
      glTexCoord2f(2.0f, 0.0f); glVertex3f(-7,-1,-7);
      glTexCoord2f(2.0f, 2.0f); glVertex3f(-7, 7,-7);
      glTexCoord2f(0.0f, 2.0f); glVertex3f( 7, 7,-7);

      glTexCoord2f(0.0f, 0.0f); glVertex3f(-7, 7, 7);   // face 3
      glTexCoord2f(2.0f, 0.0f); glVertex3f(-7, 7,-7);
      glTexCoord2f(2.0f, 2.0f); glVertex3f(-7,-1,-7);
      glTexCoord2f(0.0f, 2.0f); glVertex3f(-7,-1, 7);

      glTexCoord2f(0.0f, 0.0f); glVertex3f( 7, 7,-7);   // face 4
      glTexCoord2f(2.0f, 0.0f); glVertex3f( 7, 7, 7);
      glTexCoord2f(2.0f, 2.0f); glVertex3f( 7,-1, 7);
      glTexCoord2f(0.0f, 2.0f); glVertex3f( 7,-1,-7);

   glEnd();

   glDisable(GL_TEXTURE_2D);   // disable texturing
   glDisable(GL_CULL_FACE);    // disable face culling

   glEndList();

} // END OF CreateWorld


///////////////////////////////////////////////////////////
// Render one scene (one game frame)
///////////////////////////////////////////////////////////

void RenderScene()
{

   int i;  // loops
   int c;  // particle color
   
   GLfloat x, y, z;  // temp x y z position for particles
      
   // set up camera, clear screen, and render one scene

   glPushMatrix();  // save current scene info

   gluLookAt(Player.x, Player.y, Player.z, Player.x + (50.0f*(GLfloat)cos(Player.Angle)), Player.y + Player.Look, Player.z - (50.0f*(GLfloat)sin(Player.Angle)), 0.0f, 1.0f, 0.0f);

   // clear screen and depth buff
   glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

   glPushMatrix();                         // save matrix
   glRotatef(sky_degrees,0.0f,1.0f,0.0f);  // rotate upper sky
   glCallList(Sky2List);                   // draw upper sky (rotated)
   glPopMatrix();                          // restore matrix

   glCallList(Sky1List);                   // draw bottom blended sky (non-moving)
   glCallList(RoomList);                   // draw the room walls and floor
      
   // draw objects camo, fuel, ammo boxes
   for (i = 0; i < MAX_OBJECTS; i++)
   {
      if (Objects[i].alive)
      {
         glPushMatrix();
         
         glTranslatef(Objects[i].x, Objects[i].y, Objects[i].z);
         
         if (Objects[i].type == TYPE_CAMO) glCallList(CamoList);
         
         glPopMatrix();
      }
   } // end for

   // draw player bullets
   glColor3f(0.25f, 0.25f, 0.25f); // dark grey bullets
   for (i = 0; i < MAX_BULLETS; i++)
   {
      if (Bullets[i].alive)
      {
         glPushMatrix();
         glTranslatef(Bullets[i].x, Bullets[i].y, Bullets[i].z);
         gluSphere(game_sphere, 1.0f, 20, 20); // draw sphere rad=1 long=10 lat=10
         glPopMatrix();
      }
   } // end for
   glColor3f(1.0f, 1.0f, 1.0f); // reset color to white

   // to display player model
   //  glColor3f(1.0f, 1.0f, 1.0f);  // reset color to white
   //  glRotatef(Enemy[i].Angle, 0.0f, 1.0f, 0.0f);  // rotate on Y
   //  RenderModel(&model_copter, ENEMY_COPTER); // FIX: PASS INSTANCE OF MODEL
      
   // draw explosion particles
   for (i = 0; i < MAX_PARTICLES; i++)
   {
      if (Particles[i].alive) // if particle is alive
      {
         x = Particles[i].x; y = Particles[i].y; z = Particles[i].z; c = Particles[i].color;
         if (c == COLOR_BLK) glColor3f(0.25f, 0.25f, 0.25f);
         if (c == COLOR_WHT) glColor3f(0.90f, 0.90f, 0.90f);
         if (c == COLOR_RED) glColor3f(0.87f, 0.0f,  0.0f);
         if (c == COLOR_TAN) glColor3f(0.59f, 0.45f, 0.0f);
         if (c == COLOR_MAR) glColor3f(0.53f, 0.0f,  0.26f);
         if (c == COLOR_DGR) glColor3f(0.33f, 0.41f, 0.0f);
         if (c == COLOR_BRN) glColor3f(0.66f, 0.41f, 0.0f);
         if (c == COLOR_YEL) glColor3f(0.96f, 0.96f, 0.0f);
         
         glBegin(GL_QUADS); // draw as tiny quads
            glVertex3f(x-0.1f, y-0.1f, z);
            glVertex3f(x+0.1f, y-0.1f, z);
            glVertex3f(x+0.1f, y+0.1f, z);
            glVertex3f(x-0.1f, y+0.1f, z);
         glEnd();
      } // end if
   } // end for
   
   glColor3f(1.0f, 1.0f, 1.0f); // reset color to white

   glPopMatrix();  // restore current scene info

   DisplayScore();

   SwapBuffers(game_dc);

   sky_degrees += 0.03f;  // slowly spin the sky

   ProcessInput();
   PlayRandomSound();

} // END OF RenderScene


/////////////////////////////////////////////
// Calculate frames per second (roughly)
/////////////////////////////////////////////

void CalculateFPS()
{
   
   curr_time = GetTickCount() * 0.001f;
   FPS++; // keep track of running FPS
   if ((curr_time - prev_time) > 1.0f)  // has one second elapsed?
   {
      prev_time = curr_time; // yes save new last time
      curr_FPS = FPS;  // save current FPS
      FPS = 0;
   }
   sprintf(text,"FPS=%d", curr_FPS);  // display current FPS
   DisplayText(550, height - 30);

} // END OF CalculateFPS

