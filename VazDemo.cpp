// VazDemo a VazEngine demo by PhilVaz
// Nov 20, 2005  VazDemo with 3D models

// INCLUDES ///////////////////////////////////////////////

#include "VazDemo.h"   // game constants, structs, functions

// GLOBALS ////////////////////////////////////////////////

float player_speed  = 0.0f;      // player speed to move forward/back

int bullet_wait;  // wait time between bullets
int menu_wait;    // wait time between menu selections
int level_wait;   // wait time between new levels
int fire_wait;    // wait time between enemy fire on reset

PLAYER_STRUCT   Player;                    // Player info
BULLET_STRUCT   Bullets[MAX_BULLETS];      // Player Bullet info
OBJECT_STRUCT   Objects[MAX_OBJECTS];      // Object info
PARTICLE_STRUCT Particles[MAX_PARTICLES];  // Particle info

extern int width, height;
extern int game_state;

extern bool sound_ok;
extern int player_fire;    // fire sound buffers

extern GLuint Xmouse, Ymouse;

extern LPDIRECTSOUNDBUFFER game_sound_blaster1, game_sound_blaster2, game_sound_blaster3, game_sound_blaster4, game_sound_blaster5;
extern LPDIRECTSOUNDBUFFER game_sound_explode0;

// FUNCTIONS //////////////////////////////////////////////

void SetPlayer();
void SetObjects();  // set object boxes

void FireBullet();
void MoveBullets();

void SetEnemy();
void MoveEnemy();

void CheckCollisions();


// BEGIN GAME CODE ////////////////////////////////////////

/////////////////////////////////////////////
// Set player initial position
/////////////////////////////////////////////

void SetPlayer()
{

   int i;
   
   Player.x = 0.0f;   // center of world
   Player.z = 0.0f;
   
   Player.Look = 0.0f;  // center the view
   Ymouse = height/2 - ((int)Player.Look * 6);  // reset cursor
   
   player_speed = 0.0f; // stop movement
            
   for (i = 0; i < MAX_BULLETS;  i++)  Bullets[i].alive = FALSE;
      
      
} // END OF SetPlayer


/////////////////////////////////////////////
// Set enemy initial positions
/////////////////////////////////////////////

void SetEnemy()
{
   
              
  

} // END OF SetEnemy


/////////////////////////////////////////////
// Set objects (camos, fuels, ammos)
/////////////////////////////////////////////

void SetObjects()
{
   int i, total;

   GLfloat x, y, z;  // temp x y z position of object

   for (i = 0; i < MAX_OBJECTS; i++) Objects[i].alive = FALSE;
   for (i = 0; i < MAX_BULLETS; i++) Bullets[i].alive = FALSE;
         
   total = 10;

   y = 0.0f;

   for (i = 0; i < (total * 2); i++)
   {

      Objects[i].alive = TRUE;
      Objects[i].type = TYPE_CAMO; // camo box
      x = (GLfloat)((rand()%1960) - 980); z = (GLfloat)((rand()%1960) - 980);
      Objects[i].x = x; Objects[i].y = y; Objects[i].z = z;

   } // end for

} // END OF SetObjects


/////////////////////////////////////////////
// Move enemy A.I.
/////////////////////////////////////////////

void MoveEnemy()
{
  
  
} // END OF MoveEnemy


/////////////////////////////////////////////
// Move player bullets
/////////////////////////////////////////////

void MoveBullets()
{
   int i;
   GLfloat x, y, z;  // temp x y z position of bullet
   
   for (i = 0; i < MAX_BULLETS; i++)
   {
      if (Bullets[i].alive)
      {
         x = Bullets[i].x;   y = Bullets[i].y;   z = Bullets[i].z;   // save current position
         x += Bullets[i].dx; y += Bullets[i].dy; z += Bullets[i].dz; // move

         if (y < 6 && (x < -995 || x > 995 || z < -995 || z > 995 || y < 0))  // out of range?
         {
            Bullets[i].alive = FALSE;
            if (y > 0) InsertParticles(x,y,z,EXPLODE_WALL);  // small explosion
            else InsertParticles(x,y,z,EXPLODE_GROUND);
         }
         
         if (y > 90) Bullets[i].alive = FALSE;  // bullet thru sky, no explosion
                  
         Bullets[i].x = x; Bullets[i].y = y; Bullets[i].z = z;  // save new position
         Bullets[i].count--; if (Bullets[i].count == 0) Bullets[i].alive = FALSE;
      } // end if bullet alive
   } // end for

} // END OF MoveBullets



//////////////////////////////////////////////////
// Fires a bullet
//////////////////////////////////////////////////

void FireBullet()
{

   int i; // for loop
      
   bullet_wait--; if (bullet_wait > 0) return;
   bullet_wait = BULLET_PAUSE;
   
   for (i = 0; i < MAX_BULLETS; i++) // find a free bullet to insert
   {
      if (!Bullets[i].alive)  // found a free one
      {
         Bullets[i].alive = TRUE;
         Bullets[i].x = Player.x; Bullets[i].y = Player.y; Bullets[i].z = Player.z;
         
         Bullets[i].dx =  BULLET_SPEED * (GLfloat)cos(Player.Angle);   // x direction
         Bullets[i].dy =  Player.Look / 16.5f;                         // y direction
         Bullets[i].dz = -BULLET_SPEED * (GLfloat)sin(Player.Angle);   // z direction
         
         Bullets[i].x += (Bullets[i].dx * 1.5f);  // move bullet forward a bit
         Bullets[i].y += (Bullets[i].dy * 1.5f);
         Bullets[i].z += (Bullets[i].dz * 1.5f);
         
         Bullets[i].count = BULLET_DURATION;
        
         if (sound_ok)
         {
            player_fire++; if (player_fire > 5) player_fire = 1; // loop thru sound buffers
            if (player_fire == 1) IDirectSoundBuffer_Play(game_sound_blaster1,0,0,NULL);
            if (player_fire == 2) IDirectSoundBuffer_Play(game_sound_blaster2,0,0,NULL);
            if (player_fire == 3) IDirectSoundBuffer_Play(game_sound_blaster3,0,0,NULL);
            if (player_fire == 4) IDirectSoundBuffer_Play(game_sound_blaster4,0,0,NULL);
            if (player_fire == 5) IDirectSoundBuffer_Play(game_sound_blaster5,0,0,NULL);
         }
         
         return;
       } // end if not alive
   } // end for

} // END OF FireBullet



/////////////////////////////////////////////
// Check all potential collisions
/////////////////////////////////////////////

void CheckCollisions()
{
   int i, j;  // loop counters

   GLfloat xb, yb, zb, x, y, z;  // bullet and temp positions
      
   // check player bullets with objects and enemies
   for (i = 0; i < MAX_BULLETS; i++)
   {
      if (Bullets[i].alive)
      {
         xb = Bullets[i].x; yb = Bullets[i].y; zb = Bullets[i].z;
         // check player bullet with each object
         for (j = 0; j < MAX_OBJECTS; j++)
         {
            if (Objects[j].alive)
            {
               x = Objects[j].x; y = Objects[j].y; z = Objects[j].z;
               // check collision bullet with object
               if (yb < 7 && xb < x+7 && xb > x-7 && zb < z+7 && zb > z-7)
               {  // we have collision

                  Objects[j].alive = FALSE; Bullets[i].alive = FALSE;

                  // determine type of object for particle colors
                  if (Objects[j].type == TYPE_CAMO) InsertParticles(x,y,z,EXPLODE_CAMO);

                  PlayRandomExplosion(0);

               } // end if collision
            } // end if object alive
         } // end for objects
      
         
         
      } // end if bullet alive
   } // end for bullets
   
   // check player with objects
   for (i = 0; i < MAX_OBJECTS; i++)
   {
      if (Objects[i].alive)
      {
         x = Objects[i].x; y = Objects[i].y; z = Objects[i].z;
         // check collision player with object
         if (Player.x < x+9 && Player.x > x-9 && Player.z < z+9 && Player.z > z-9)
         {
            Objects[i].alive = FALSE;

            if (sound_ok)
            {
               if (Objects[i].type == TYPE_CAMO) IDirectSoundBuffer_Play(game_sound_explode0,0,0,NULL);
               
            }
            
         } // end if collision
      } // end if object alive
   } // end for objects
      

   
   
} // END OF CheckCollisions

// END GAME CODE //////////////////////////////////////////