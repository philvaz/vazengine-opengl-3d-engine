// VazPart.cpp by PhilVaz
// VazEngine particle system
// Nov 20, 2005

#include "VazPart.h"

extern PARTICLE_STRUCT Particles[MAX_PARTICLES];

/////////////////////////////////////////////
// Insert particle explosion of type t
/////////////////////////////////////////////

void InsertParticles(GLfloat x, GLfloat y, GLfloat z, int t)
{
   int i, n, p, r;
      
   if (t == EXPLODE_WALL || EXPLODE_GROUND) p = 1;
   if (t == EXPLODE_CAMO) p = 2;
   
   n = p * 300 + (rand()%(p*300)); // number of particles to insert
      
   for (i = 0; i < MAX_PARTICLES; i++)
   {
      if (!Particles[i].alive) // found free particle
      {
         Particles[i].alive = TRUE;
         
         Particles[i].x = x; // position at center of collision
         Particles[i].y = y;
         Particles[i].z = z;
         
         Particles[i].dx = 0.5f * p * ((GLfloat)(rand()%10000) / 10000.0f) - .25f * p;  // movement away from center
         Particles[i].dy = 0.5f * p * ((GLfloat)(rand()%10000) / 10000.0f) - .25f * p;
         Particles[i].dz = 0.5f * p * ((GLfloat)(rand()%10000) / 10000.0f) - .25f * p;
         
         if (t == EXPLODE_WALL)
         {
            r = (rand()%2);
            if (r == 0) Particles[i].color = COLOR_BLK;
            if (r == 1) Particles[i].color = COLOR_WHT;
         }
         
         if (t == EXPLODE_GROUND)
         {
            r = (rand()%2);
            if (r == 0) Particles[i].color = COLOR_TAN;
            if (r == 1) Particles[i].color = COLOR_BRN;
         
         }

         if (t == EXPLODE_CAMO)
         {
            r = (rand()%4);
            if (r == 0) Particles[i].color = COLOR_BLK;
            if (r == 1) Particles[i].color = COLOR_WHT;
            if (r == 2) Particles[i].color = COLOR_MAR;
            if (r == 3) Particles[i].color = COLOR_DGR;
         
         }
         
         Particles[i].count = p * 50 + (rand()%(p*50));  // set duration of particle
                  
         n--; if (n == 0) return;  // finished inserting all particles
      } // end if
   } // end for

} // END OF InsertParticles


/////////////////////////////////////////////
// Move all particles
/////////////////////////////////////////////

void MoveParticles()
{
   int i;
   GLfloat x, y, z;  // temp x y z position of particle
   
   for (i = 0; i < MAX_PARTICLES; i++)
   {
      if (Particles[i].alive)
      {
         // save current position
         x = Particles[i].x; y = Particles[i].y; z = Particles[i].z;
         
         // move to new position
         x += Particles[i].dx; y += Particles[i].dy; z += Particles[i].dz;
         
         // gravity effect on y movement
         if (Particles[i].dy < 0) Particles[i].dy += GRAVITY_SPEED;
         else Particles[i].dy -= GRAVITY_SPEED;
         
         if (x < -998 || x > 998) { Particles[i].dx *= (-1); x += Particles[i].dx; } // bounce on x
         if (z < -998 || z > 998) { Particles[i].dz *= (-1); z += Particles[i].dz; } // bounce on z
         if (y < 0) { Particles[i].dy *= (-1);  y += Particles[i].dy; } // bounce on y (ground)
                  
         // save new position
         Particles[i].x = x; Particles[i].y = y; Particles[i].z = z;
         
         // check if particle finished
         Particles[i].count--; if (Particles[i].count == 0) Particles[i].alive = FALSE;
         
      } // end if
      
   } // end for

} // END OF MoveParticles

