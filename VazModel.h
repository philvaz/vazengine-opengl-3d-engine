#ifndef _VAZMODEL_H
#define _VAZMODEL_H

// VazModel.h by PhilVaz
// VazEngine model animate and render functions
// Nov 20, 2005

// HEADER FILE ///////////////////////////////////////////////

#define WIN32_LEAN_AND_MEAN

#include <windows.h>
#include <windowsx.h>
#include <mmsystem.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#include <gl\gl.h>     // OpenGL32 library
#include <gl\glu.h>    // GLU32    library
#include <gl\glaux.h>  // GLAUX    library

#include "Vaz2D.h"
#include "Vaz3D.h"
#include "VazGame.h"
#include "VazInput.h"
//#include "VazModel.h"
#include "VazPart.h"
#include "VazSound.h"
#include "WIN32.h"

#include "MD2.h"
#include "VazDemo.h"

bool  TextureModel(UINT texArray[], LPSTR filename, int texID);

void  RenderModel(t3DModel *pModel, int r);  // render model by render type

float ReturnTime(t3DModel *pModel, int nextFrame);  // for interpolation anim times

bool  LoadModels();  // loads MD2 models and textures

#endif
