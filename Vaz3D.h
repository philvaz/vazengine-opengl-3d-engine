#ifndef _VAZ3D_H
#define _VAZ3D_H

// Vaz3D.h by PhilVaz
// VazEngine 3D graphics render functions
// Nov 20, 2005

// HEADER FILE ///////////////////////////////////////////////

#define WIN32_LEAN_AND_MEAN

#include <windows.h>
#include <windowsx.h>
#include <mmsystem.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#include <gl\gl.h>     // OpenGL32 library
#include <gl\glu.h>    // GLU32    library
#include <gl\glaux.h>  // GLAUX    library

#include "Vaz2D.h"
//#include "Vaz3D.h"
#include "VazGame.h"
#include "VazInput.h"
#include "VazModel.h"
#include "VazPart.h"
#include "VazSound.h"
#include "WIN32.h"

#include "MD2.h"
#include "VazDemo.h"

// functions

bool DrawInit();

void DrawQuit();

void MoveCamera(GLdouble steps);           // move camera in position

AUX_RGBImageRec *LoadBMP(char *Filename);  // load BMP requires glaux library

bool LoadTextures();

void CreateWorld();

void RenderScene();

void CalculateFPS();

#endif
