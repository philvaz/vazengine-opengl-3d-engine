#ifndef _VAZSOUND_H
#define _VAZSOUND_H

// VazSound.h by PhilVaz
// VazEngine sound functions
// Nov 20, 2005

// HEADER FILE ///////////////////////////////////////////////

#define WIN32_LEAN_AND_MEAN

#include <windows.h>
#include <windowsx.h>
#include <mmsystem.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#include <gl\gl.h>     // OpenGL32 library
#include <gl\glu.h>    // GLU32    library
#include <gl\glaux.h>  // GLAUX    library

#include "Vaz2D.h"
#include "Vaz3D.h"
#include "VazGame.h"
#include "VazInput.h"
#include "VazModel.h"
#include "VazPart.h"
//#include "VazSound.h"
#include "WIN32.h"

#include "MD2.h"
#include "VazDemo.h"

#include <dsound.h>    // include DirectSound
#include "dsutil.h"    // for LoadSoundBuffer()

// functions

bool SoundInit();

void SoundQuit();

void PlayRandomSound();           // adds background war noise

void PlayRandomExplosion(int);    // explosion noise (0=soft, 1=louder, etc)

#endif
