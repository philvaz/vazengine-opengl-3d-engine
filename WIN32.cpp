// WIN32.cpp by PhilVaz
// Nov 20, 2005
// VazEngine window functions WinMain, WinProc

#include "WIN32.h"

// globals

HWND      game_window   = NULL;  // global game window handle
HINSTANCE game_instance = NULL;  // global game instance handle

extern int game_state, width, height;

extern UCHAR key_data[256];

// functions

// WINPROC ////////////////////////////////////////////////

LRESULT CALLBACK WinProc(HWND hwnd, UINT msg, WPARAM wparam, LPARAM lparam)
{  // this is the main message handler of the system

   HDC         hdc; // handle to a device context
   PAINTSTRUCT ps;  // used in WM_PAINT

   switch(msg) // what is the message
   {
      case WM_CREATE:
      {
         // do initialization stuff here

         return(0); // return success
      } break;

      case WM_PAINT:
      {
         hdc = BeginPaint(hwnd, &ps); // validate the window

         EndPaint(hwnd, &ps);

         return(0); // return success
      }  break;

      case WM_DESTROY:
      {
         PostQuitMessage(0); // kill the application, sends a WM_QUIT message

         return(0); // return success
      }  break;

      default:break;

   } // end switch

// process any messages that we didn't take care of

return (DefWindowProc(hwnd, msg, wparam, lparam));

} // end WinProc


// WINMAIN ////////////////////////////////////////////////

int WINAPI WinMain(HINSTANCE hinstance, HINSTANCE hprevinstance, LPSTR lpcmdline, int ncmdshow)
{

   WNDCLASSEX winclass; // this will hold the class we create
   HWND hwnd;           // generic window handle
   MSG msg;             // generic message

   // first fill in the window class structure

   winclass.cbSize        = sizeof(WNDCLASSEX);
   winclass.style         = CS_DBLCLKS | CS_OWNDC |
                            CS_HREDRAW | CS_VREDRAW;
   winclass.lpfnWndProc   = WinProc;
   winclass.cbClsExtra    = 0;
   winclass.cbWndExtra    = 0;
   winclass.hInstance     = hinstance;
   winclass.hIcon         = LoadIcon(NULL, IDI_APPLICATION);
   winclass.hIconSm       = LoadIcon(NULL, IDI_APPLICATION);
   winclass.hCursor       = LoadCursor(NULL, IDC_ARROW);
   winclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
   winclass.lpszMenuName  = NULL;
   winclass.lpszClassName = WINDOW_CLASS_NAME;

   // save the game instance handle
   game_instance = hinstance;

   // register the window class
   if (!RegisterClassEx(&winclass)) return(0);

   // create the window
   if (!(hwnd = CreateWindowEx(NULL,                   // extended style
                               WINDOW_CLASS_NAME,      // class
                               "VazDemo",              // title
                               WS_POPUP | WS_VISIBLE,  // use POPUP for full screen
                               0,0,           // initial game window x,y
                               width,         // initial game width
                               height,        // initial game height
                               NULL,          // handle to parent
                               NULL,          // handle to menu
                               hinstance,     // instance of this application
                               NULL)))        // extra creation parms
   return(0);

   // save the game window handle
   game_window = hwnd;

   GameInit();   // game initialization function called here

   // enter main event loop using PeekMessage() to retrieve messages

   while(TRUE)
   {

      // get initial tick count to keep game speed constant
      DWORD start_tick = GetTickCount();

      // is there a message in queue, if so get it
      if (PeekMessage(&msg,NULL,0,0,PM_REMOVE))
      {
         // test if this is a quit
         if (msg.message == WM_QUIT) break;

         // translate any accelerator keys
         TranslateMessage(&msg);

         // send the message to WinProc
         DispatchMessage(&msg);

      } // end if

      GameMain();  // game main processing function called here

      // check for <ESC> key and send quit game
      if (KEYDOWN(key_data, DIK_ESCAPE)) SendMessage (game_window, WM_CLOSE, 0, 0);

      // wait until we hit correct game speed frame rate
      while ((GetTickCount() - start_tick) < GAME_SPEED);

   } // end while

   GameQuit();  // game quit function and clean up before exit called here

   return(msg.wParam); // return to Windows

} // end WinMain