#ifndef _VAZINPUT_H
#define _VAZINPUT_H

#define DIRECTINPUT_VERSION 0x0800

// VazInput.h by PhilVaz
// VazEngine input functions (keyboard, mouse, joystick)
// Nov 20, 2005

// HEADER FILE ///////////////////////////////////////////////

#define WIN32_LEAN_AND_MEAN

#include <windows.h>
#include <windowsx.h>
#include <mmsystem.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#include <gl\gl.h>     // OpenGL32 library
#include <gl\glu.h>    // GLU32    library
#include <gl\glaux.h>  // GLAUX    library

#include "Vaz2D.h"
#include "Vaz3D.h"
#include "VazGame.h"
//#include "VazInput.h"
#include "VazModel.h"
#include "VazPart.h"
#include "VazSound.h"
#include "WIN32.h"

#include "MD2.h"
#include "VazDemo.h"

#include <dinput.h>

// defines (macros)

#define KEYDOWN(name,key)    (name[key] & 0x80)

// functions

bool InputInit();
void InputQuit();
void ProcessInput();  // process all keyboard and mouse input

bool KeyInit();
void KeyStatus();

bool MouseInit();
void MouseStatus();

bool JoyInit();
void JoyStatus();

#endif