// Vaz2D.cpp by PhilVaz
// VazEngine 2D text, menu, HUD functions
// Nov 20, 2005

#include "Vaz2D.h"

// globals

GLuint font_base;        // for game font

char text[80];           // for display text output 80 chars max

extern int width, height;
extern int game_state;
extern bool sound_ok;

extern HWND game_window;

extern PLAYER_STRUCT Player;
extern BULLET_STRUCT Bullets[MAX_BULLETS];
extern OBJECT_STRUCT Objects[MAX_OBJECTS];

extern int joy_up, joy_down, joy_left, joy_right;

extern GLuint texture[MAX_TEXS];

extern GLuint Xmouse, Ymouse;


// functions

////////////////////////////////////////////////////////
// Displays 2D text at x,y
////////////////////////////////////////////////////////

void DisplayText(int x, int y)
{

   glRasterPos2i(x,y);
   glPushAttrib(GL_LIST_BIT);
      glListBase(font_base - 32);
      glCallLists(strlen(text), GL_UNSIGNED_BYTE, text);
   glPopAttrib();

} // END OF DisplayText


////////////////////////////////////////////////////////
// Displays the score and other at bottom of screen
////////////////////////////////////////////////////////

void DisplayScore()
{
   
   // set the text string and display text

   glMatrixMode(GL_PROJECTION);
   glPushMatrix();   // save proj matrix
   glLoadIdentity(); // reset

   glOrtho(0, width, height, 0, -1, 1); // set up 2D display

   glMatrixMode(GL_MODELVIEW);
   glPushMatrix();   // save model matrix
   glLoadIdentity(); // reset

   sprintf(text,"Xm = %d   ", Xmouse);
   DisplayText(20, 20);
   
   sprintf(text, "Ym = %d   ", Ymouse);
   DisplayText(120, 20);
   
   sprintf(text, "Angle = %f   ", (float)Player.Angle);
   DisplayText(250, 20);
   
   sprintf(text, "Look = %f   ", (float)Player.Look);
   DisplayText(440, 20);
   
   
   // display menu in demo only
   if (game_state == GAME_STATE_DEMO_RUN) DisplayMenu();

   // display target cross-hair and 2D bitmap in game run only
   if (game_state == GAME_STATE_GAME_RUN)
   {
      sprintf(text,"+");
      DisplayText(width/2 - 2, height/2 + 2);
      
      // display any 2D bitmaps here

   }

   DisplayRadar(); // both demo and game run radar
   
   CalculateFPS();

   // restore proj and model matrices
   glMatrixMode(GL_PROJECTION);
   glPopMatrix();
   glMatrixMode(GL_MODELVIEW);
   glPopMatrix();

} // END OF DisplayScore


///////////////////////////////////////////////////////////
// Display radar bottom-left of screen
///////////////////////////////////////////////////////////

void DisplayRadar()
{
   int i, xi, yi;
   GLfloat x,z;
      
   // radar is normal
   
   glColor3f(0.0f, 0.0f, 1.0f); // blue player
   
   x = Player.x; z = Player.z;
   xi = (int)((x + 1000) / 20) + 5;  // convert 2D points
   yi = (int)(((z + 1000) / 20) + height - 105);
      
   glBegin(GL_QUADS);  // display player
      glVertex2i(xi-5, yi-5);
      glVertex2i(xi+5, yi-5);
      glVertex2i(xi+5, yi+5);
      glVertex2i(xi-5, yi+5);
   glEnd();
   
   for (i = 0; i < MAX_BULLETS; i++) // loop thru player bullets
   {
      if (Bullets[i].alive)
      {
         x = Bullets[i].x; z = Bullets[i].z;
         xi = (int)((x + 1000) / 20) + 5; // convert 2D points
         yi = (int)(((z + 1000) / 20) + height - 105);
               
         glBegin(GL_QUADS);  // display bullets as quads
            glVertex2i(xi-2, yi-2);
            glVertex2i(xi+2, yi-2);
            glVertex2i(xi+2, yi+2);
            glVertex2i(xi-2, yi+2);
         glEnd();
      } // end if
   } // end for
   
   glColor3f(0.0f, 1.0f, 0.0f); // green objects
   
   for (i = 0; i < MAX_OBJECTS; i++) // loop thru objects
   {
      if (Objects[i].alive)
      {
         x = Objects[i].x; z = Objects[i].z;
         xi = (int)((x + 1000) / 20) + 5;
         yi = (int)(((z + 1000) / 20) + height - 105);
         
         glBegin(GL_LINE_LOOP);
            glVertex2i(xi-3, yi-3);
            glVertex2i(xi+3, yi-3);
            glVertex2i(xi+3, yi+3);
            glVertex2i(xi-3, yi+3);
         glEnd();
         
      } // end if
      
   } // end for
      
   glColor3f(1.0f, 1.0f, 1.0f); // reset color to white
         
} // END OF DisplayRadar


///////////////////////////////////////////////////////////
// Display menu selections
///////////////////////////////////////////////////////////

void DisplayMenu()
{

   glColor3f(1.0f, 1.0f, 1.0f);
         
   // HEADING

   sprintf(text,"VazEngine Demo 1.0 by PhilVaz -- Press Space");
   DisplayText(125, height - 30);
      
   glColor3f(1.0f, 1.0f, 1.0f); // reset color to white

} // END OF DisplayMenu


///////////////////////////////////////////////////////////
// Process menu selections
///////////////////////////////////////////////////////////

void ProcessMenu()
{


   


} // END OF ProcessMenu