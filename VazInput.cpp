// VazInput.cpp by PhilVaz
// VazEngine input functions (keyboard, mouse, joystick)
// Nov 20, 2005

#include "VazInput.h"

// globals

extern int width, height;

GLuint Xmouse = width - 2;    // mouse X position
GLuint Ymouse = height / 2;   // mouse Y position

int joy_left, joy_right, joy_up, joy_down, joy_but1, joy_but2, joy_but3, joy_but4;

bool key_ok   = FALSE;
bool mouse_ok = FALSE;
bool joy_ok   = FALSE;

LPDIRECTINPUT8 game_input_main;

LPDIRECTINPUTDEVICE8 key_main;
LPDIRECTINPUTDEVICE8 mouse_main;
LPDIRECTINPUTDEVICE8 joy_main;

UCHAR key_data[256];
DIMOUSESTATE mouse_data;
DIJOYSTATE joy_data;

extern bool input_ok, sound_ok;

extern double PI;

extern int game_state;

extern float player_speed;

extern PLAYER_STRUCT Player;

extern HWND game_window;

extern HINSTANCE game_instance;


///////////////////////////////////////////////////
// Initialize DirectInput
///////////////////////////////////////////////////

bool InputInit()
{

   // Create Direct Input main object
   if (DirectInput8Create(game_instance, DIRECTINPUT_VERSION, IID_IDirectInput8, (void **)&game_input_main, NULL) != DI_OK)
   {
      MessageBox(game_window, "Input could not be initialized -- Direct Input Create Error","Input Error",MB_OK);
      return FALSE;
   }

   key_ok   = KeyInit();
   mouse_ok = MouseInit();
   joy_ok   = JoyInit();
   
   return TRUE;

} // END OF InputInit

///////////////////////////////////////////////////
// Read Keyboard Status
///////////////////////////////////////////////////

void InputQuit()
{

   // if keyboard acquired, release keyboard
   if (key_main)
   {
      IDirectInputDevice8_Unacquire(key_main);
      IDirectInputDevice8_Release(key_main);
      key_main = NULL;
   }

   // if mouse acquired, release mouse
   if (mouse_main)
   {
      IDirectInputDevice8_Unacquire(mouse_main);
      IDirectInputDevice8_Release(mouse_main);
      mouse_main = NULL;
   }
   
   // release joystick here too
   

   // release main DirectInput object
   if (game_input_main) IDirectInput8_Release(game_input_main);

} // END OF InputQuit


////////////////////////////////////////////
// Process key and mouse input
////////////////////////////////////////////

void ProcessInput()
{

   KeyStatus();    // read the keyboard
   MouseStatus();  // read the mouse
   JoyStatus();    // read the joystick
   
   if (game_state == GAME_STATE_DEMO_RUN) ProcessMenu();
      
   // up arrow or right mouse increases speed forward
   if (game_state == GAME_STATE_GAME_RUN && (KEYDOWN(key_data, DIK_UP) || (mouse_data.rgbButtons[1] & 0x80) || KEYDOWN(key_data, DIK_W) || joy_but1))
   {
      if (player_speed == 0) player_speed = PLAYER_SPEED_MIN;
      player_speed += PLAYER_SPEED_INC; if (player_speed > PLAYER_SPEED_MAX) player_speed = PLAYER_SPEED_MAX;
   }

   // down arrow decreases speed back
   else if (game_state == GAME_STATE_GAME_RUN && (KEYDOWN(key_data, DIK_DOWN) || KEYDOWN(key_data, DIK_S) || joy_but3))
   {
      if (player_speed == 0) player_speed = -PLAYER_SPEED_MIN;
      player_speed -= PLAYER_SPEED_INC; if (player_speed < -PLAYER_SPEED_MAX) player_speed = -PLAYER_SPEED_MAX;
   }

   if (game_state == GAME_STATE_GAME_RUN && (KEYDOWN(key_data, DIK_X) || joy_but4)) // X or But4 is Stop
   {
      player_speed = 0;
   }
   
   // if we have speed move
   if ((player_speed != 0) || game_state == GAME_STATE_DEMO_RUN)
   {
      MoveCamera(player_speed);
      if (game_state == GAME_STATE_GAME_RUN)
      {
         // sound run looping
      }
   }
   else
   {
      player_speed = 0; 
   } // end else
   
   if (game_state == GAME_STATE_GAME_RUN && (KEYDOWN(key_data, DIK_RIGHT) || KEYDOWN(key_data, DIK_D) || joy_right)) // rotate right
   {
      Player.Angle -= ROTATE_SPEED / (PI*PI);
      if (Player.Angle < 0.0f) Player.Angle = 6.27f;  // avoids errors
      Xmouse = width - 1 - (int)(Player.Angle * 101.7f);  // reset cursor
   }

   else if (game_state == GAME_STATE_GAME_RUN && (KEYDOWN(key_data, DIK_LEFT) || KEYDOWN(key_data, DIK_A) || joy_left)) // rotate left
   {
      Player.Angle += ROTATE_SPEED / (PI*PI);
      if (Player.Angle > (2.0f * PI)) Player.Angle = 0.03f;              // avoids errors
      Xmouse = width - 1 - (int)(Player.Angle * 101.7f);  // reset cursor
   }

   if (game_state == GAME_STATE_GAME_RUN && (KEYDOWN(key_data, DIK_Z) || joy_down))  // Z looks down
   {
      Player.Look -= 1.0f;
      if (Player.Look < -40.0f) Player.Look = -40.0f;           // limit angle down
      Ymouse = height/2 - ((int)Player.Look * 6);  // reset cursor
   }

   else if (game_state == GAME_STATE_GAME_RUN && (KEYDOWN(key_data, DIK_Q) || joy_up))  // Q looks up
   {
      Player.Look += 1.0f;
      if (Player.Look > 40.0f) Player.Look = 40.0f;           // limit amgle up
      Ymouse = height/2 - ((int)Player.Look * 6);  // reset cursor
   }

   else if (game_state == GAME_STATE_GAME_RUN && KEYDOWN(key_data, DIK_END))  // end key is center
   {
      Player.Look = 0.0f;  // center the view
      Ymouse = height/2;  // reset cursor
   }

   if (game_state == GAME_STATE_GAME_RUN && (KEYDOWN(key_data, DIK_LCONTROL) || (mouse_data.rgbButtons[0] & 0x80) || joy_but2)) FireBullet();  // fire a bullet

} // END OF ProcessInput


///////////////////////////////////////////////////
// Initialize Keyboard
///////////////////////////////////////////////////

bool KeyInit()
{

   // Direct Input object succeeded so create device
   if (IDirectInput8_CreateDevice(game_input_main, GUID_SysKeyboard, &key_main, NULL) != DI_OK)
   {
      MessageBox(game_window, "Keyboard input could not be initialized -- Create Device Error","Input Error",MB_OK);
      return FALSE;
   }

   // Set coop level for device
   if (IDirectInputDevice8_SetCooperativeLevel(key_main, game_window, DISCL_NONEXCLUSIVE | DISCL_BACKGROUND) != DI_OK)
   {
      MessageBox(game_window, "Keyboard could not be initialized -- Cooperative Level Error","Input Error",MB_OK);
      return FALSE;
   }

   // Set data format for device
   if (IDirectInputDevice8_SetDataFormat(key_main, &c_dfDIKeyboard) != DI_OK)
   {
      MessageBox(game_window, "Keyboard could not be initialized -- Set Data Format Error","Input Error",MB_OK);
      return FALSE;
   }

   // Acquire device
   if (IDirectInputDevice8_Acquire(key_main) != DI_OK)
   {
      MessageBox(game_window, "Keyboard could not be initialized -- Could Not Acquire Error","Input Error",MB_OK);
      return FALSE;
   }

   // keyboard init OK and acquired
   return TRUE;

} // END OF KeyInit

///////////////////////////////////////////////////
// Read Keyboard Status
///////////////////////////////////////////////////

void KeyStatus()
{

   if (IDirectInputDevice8_GetDeviceState(key_main, sizeof(UCHAR[256]), (LPVOID)key_data) == DI_OK)
   {
      key_ok = TRUE;
   }
   else  // keyboard is unavailable (unacquired)
   {
      memset(key_data,0,sizeof(key_data)); // clear key data
      key_ok = FALSE;
   }

} // END OF KeyStatus


///////////////////////////////////////////////////
// Initialize Mouse
///////////////////////////////////////////////////

bool MouseInit()
{

   // Direct Input object succeeded so create device
   if (IDirectInput8_CreateDevice(game_input_main, GUID_SysMouse, &mouse_main, NULL) != DI_OK)
   {
      MessageBox(game_window, "Mouse input could not be initialized -- Create Device Error","Input Error",MB_OK);
      return FALSE;
   }

   // Set coop level for devices
   if (IDirectInputDevice8_SetCooperativeLevel(mouse_main, game_window, DISCL_NONEXCLUSIVE | DISCL_BACKGROUND) != DI_OK)
   {
      MessageBox(game_window, "Mouse could not be initialized -- Cooperative Level Error","Input Error",MB_OK);
      return FALSE;
   }

   // Set data format for device
   if (IDirectInputDevice8_SetDataFormat(mouse_main, &c_dfDIMouse) != DI_OK)
   {
      MessageBox(game_window, "Mouse could not be initialized -- Set Data Format Error","Input Error",MB_OK);
      return FALSE;
   }

   // Acquire device
   if (IDirectInputDevice8_Acquire(mouse_main) != DI_OK)
   {
      MessageBox(game_window, "Mouse could not be initialized -- Could Not Acquire Error","Input Error",MB_OK);
      return FALSE;
   }

   // mouse init OK and acquired
   return TRUE;

} // END OF MouseInit

///////////////////////////////////////////////////
// Read Mouse Status and Position
///////////////////////////////////////////////////

void MouseStatus()
{

   int Xmove, Ymove;
      
   if (IDirectInputDevice8_GetDeviceState(mouse_main, sizeof(DIMOUSESTATE), (LPVOID)&mouse_data) == DI_OK)
   {
      // get mouse X Y values
      Xmove = (int)mouse_data.lX;
      Ymove = (int)mouse_data.lY;
      mouse_ok = TRUE;
      
      Xmouse += Xmove;
      if (Xmouse < 1) Xmouse = width - 2;
      if (Xmouse > (width - 2)) Xmouse = 1;
            
      Ymouse += Ymove;
      if (Ymouse < 1) Ymouse = 1;
      if (Ymouse > (height - 1)) Ymouse = height - 1;
            	     
      Player.Angle = (width - 1 - Xmouse) / 101.7f;   // X view angle based on X pos
      Player.Look = (height / 2 - (float)Ymouse) / 6; // Y view angle based on Y pos
	     
      //if ((int)Xmouse > width - 2) SetCursorPos(1, Ymouse);  // too far right wrap left
      //if (Xmouse < 1) SetCursorPos(width - 2, Ymouse);       // too far left  wrap right

   } // end if mouse ok

   else // mouse is unavailable (unacquired)
   {
      memset(&mouse_data,0,sizeof(mouse_data));  // clear mouse data
      mouse_ok = FALSE;
   } // end else

} // END OF MouseStatus


///////////////////////////////////////
// Initialize Joystick
///////////////////////////////////////

bool JoyInit()
{

   // clear joystick status
   joy_left = 0; joy_right = 0; joy_up = 0; joy_down = 0;
   joy_but1 = 0; joy_but2 = 0; joy_but3 = 0; joy_but4 = 0;

   // init DirectInput for Joystick here

   

   return FALSE;  // change to return TRUE when DirectInput read for Joystick

} // END OF JoyInit

///////////////////////////////////////////////
// Read Joystick Status and Position
///////////////////////////////////////////////

void JoyStatus()
{
   if (joy_ok)
   {
      // check horizontal movement
      joy_left = 0; joy_right = 0;
      
      
      
      // check vertical movement
      joy_up = 0; joy_down = 0;
      
      

      // check four buttons
      joy_but1 = 0; joy_but2 = 0; joy_but3 = 0; joy_but4 = 0;
      
      
      

   } // end if joy ok


} // END OF JoyStatus