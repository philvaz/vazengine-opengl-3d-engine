// MD2.cpp by PhilVaz (based on GameTutorials.com)
// Loads in MD2 Model file (see MD2.h definitions)
// The MD2 Model definition and Quake 2 Game are owned by Id Software

// Oct 30, 2005

#include "MD2.h"

//////////////////////////////////////////////////////////////////////
// Class constructor that inits the MD2 model structs
//////////////////////////////////////////////////////////////////////

CLoadMD2::CLoadMD2()
{
   memset(&Header, 0, sizeof(tMD2Header));  // clear header struct

   pSkins    = NULL;  // clear skin, texcoords, tris, anim frames
   pTexVerts = NULL;
   pTris     = NULL;
   pFrames   = NULL;

} // end constructor


////////////////////////////////////////////////////////////////////////
// Functions to open, read, and clean up MD2 file data
////////////////////////////////////////////////////////////////////////

bool CLoadMD2::ImportMD2(t3DModel *pModel, char *filename, char *texname)
{
   char errormsg[255] = {0};

   FilePointer = fopen(filename, "rb");  // open MD2 file for binary reading

   if (!FilePointer)  // does MD2 file exist?
   {
      // if not, display error message and quit
      sprintf(errormsg, "Missing the MD2 model file: %s", filename);
      MessageBox(NULL, errormsg, "LoadMD2Model Error", MB_OK);
      return FALSE;
   } // end if

   fread(&Header, 1, sizeof(tMD2Header), FilePointer);   // file exists, read header data

   if (Header.version != 8)  // do we have version 8 ?
   {
      // if not, display error message and quit
      sprintf(errormsg, "Invalid MD2 model file (version not 8): %s", filename);
      MessageBox(NULL, errormsg, "LoadMD2Model Error", MB_OK);
      return FALSE;
   } // end if

   ReadMD2Data();  // valid MD2, now read in model and anim data

   ConvertDataStructures(pModel);  // convert and save MD2 data in model struct

   if (texname)  // if valid texture name, save texture data
   {
      tMaterialInfo texture;               // create material info struct
      strcpy(texture.filename, texname);   // save texture file name
      texture.texID = 0;                   // only one texture for MD2 so ID = 0
      texture.uTile = texture.uTile = 1;   // scale for UV coords is 1 to 1
      pModel->numMats = 1;                 // one material for model
      pModel->pMats.push_back(texture);    // save material info
   } // end if

   CleanUp();  // clean up when done

   return TRUE;  // successfully loaded MD2 file

} // end ImportMD2


////////////////////////////////////////////////////////////////////
// Reads MD2 model data
////////////////////////////////////////////////////////////////////

void CLoadMD2::ReadMD2Data()
{
   int i, j;  // for loops

   unsigned char buffer[MD2_MAX_FRAMESIZE];  // space for frames of animation

   pSkins     = new tMD2Skin [Header.numSkins];          // memory for skins
   pTexVerts  = new tMD2TexVert [Header.numTexVerts];  // memory for texcoords
   pTris      = new tMD2Face [Header.numTris];      // memory for face tris
   pFrames    = new tMD2Frame [Header.numFrames];        // memory for anim frames

   fseek(FilePointer, Header.offsetSkins, SEEK_SET);  // read to first skin name
   fread(pSkins, sizeof(tMD2Skin), Header.numSkins, FilePointer);

   fseek(FilePointer, Header.offsetTexVerts, SEEK_SET);  // read to texcoords
   fread(pTexVerts, sizeof(tMD2TexVert), Header.numTexVerts, FilePointer);

   fseek(FilePointer, Header.offsetTris, SEEK_SET);  // read to tri data
   fread(pTris, sizeof(tMD2Face), Header.numTris, FilePointer);

   fseek(FilePointer, Header.offsetFrames, SEEK_SET);  // read to vertex keyframes

   for (i = 0; i < Header.numFrames; i++)  // read all keyframes
   {
      tMD2AliasFrame *pFrame = (tMD2AliasFrame *) buffer;  // assign alias frame to buffer
      pFrames[i].pVerts = new tMD2Triangle [Header.numVerts];  // memory for first frame vertices
      fread(pFrame, 1, Header.frameSize, FilePointer);  // read in first animation frame
      strcpy(pFrames[i].name, pFrame->name);  // copy name of anim to array
      tMD2Triangle *pVerts = pFrames[i].pVerts;  // save vertex array pointer

      for (j = 0; j < Header.numVerts; j++)  // loop for all remaining anim frames
      {
         // negate Z axis, swap Y and Z
         pVerts[j].vertex[0] = pFrame->aliasVerts[j].vertex[0] * pFrame->scale[0] + pFrame->translate[0];
         pVerts[j].vertex[2] = -1 * (pFrame->aliasVerts[j].vertex[1] * pFrame->scale[1] + pFrame->translate[1]);
         pVerts[j].vertex[1] = pFrame->aliasVerts[j].vertex[2] * pFrame->scale[2] + pFrame->translate[2];
      } // end for j

   } // end for i

} // end ReadMD2Data


/////////////////////////////////////////////////////////////////////////////////////
//
// Parse and read in animations
//
// The standard MD2 (Quake 2) model keyframe animations are
//
//    Animation   Text Name              Frames
//     0          STANDING IDLE          0-39
//     1          RUN                    40-45
//     2          ATTACK                 46-53
//     3          PAIN 1                 54-57
//     4          PAIN 2                 58-61
//     5          PAIN 3                 62-65
//     6          JUMP                   66-71
//     7          FLIP                   72-83
//     8          SALUTE                 84-94
//     9          TAUNT                  95-111
//    10          WAVE                   112-122
//    11          POINT                  123-134
//    12          CROUCH STAND           135-153
//    13          CROUCH WALK            154-159
//    14          CROUCH ATTACK          160-168
//    15          CROUCH PAIN            169-172
//    16          CROUCH DEATH           173-177
//    17          DEATH BACK             178-183
//    18          DEATH FORWARD          184-189
//    19          DEATH SLOW             190-197
//
// See Tricks of the 3D Game Programming Gurus by Andre Lamothe (2003), page 1514
// Also pages 1493-1535 for a good explanation of the MD2 (Quake 2) model format
//
// This standard MD2 animation is not strictly followed in the implementation below
// but it should work for any valid MD2 file with these animations
//
/////////////////////////////////////////////////////////////////////////////////////

void CLoadMD2::ParseAnimations(t3DModel *pModel)
{
   int i;  // for loop

   tAnimationInfo anim;    // animation struct
   string lastname = "";   // saves last animation name
   
   int startFrame = 0;     // set initial start frame
         
   for (i = 0; i < pModel->numFrames; i++)  // parse each frame of animation
   {
      string name  = pFrames[i].name;  // assign name for animation frame
      
      name = name.substr(0, name.length() - 2);  // delete end 2 digits
      
      if (name != lastname && i > 0)  // do we have a new animation?
      {
         strcpy(anim.name, lastname.c_str());  // last name saved is new anim name
         anim.endFrame = i;                    // save new end frame
         anim.startFrame = startFrame;         // save start frame

         startFrame = i;  // current frame is new start frame

         pModel->pAnims.push_back(anim);  // save anim data
         pModel->numAnims++;              // increment total anims
                           
         memset(&anim, 0, sizeof(tAnimationInfo));  // clear anim data

      }

      lastname = name;  // save current anim name to compare later
      
   } // end for i
   
   strcpy(anim.name, lastname.c_str());  // save last animation
   anim.endFrame = i;                    // end frame
   anim.startFrame = startFrame;         // start frame
      
   pModel->pAnims.push_back(anim);  // save anim data
   pModel->numAnims++;              // increment total anims

} // end ParseAnimations


/////////////////////////////////////////////////////////////////////////////////////
// Convert MD2 structs to our model structs
/////////////////////////////////////////////////////////////////////////////////////

void CLoadMD2::ConvertDataStructures(t3DModel *pModel)
{
   int i, j;  // for loops

   memset(pModel, 0, sizeof(t3DModel));  // clear model struct

   pModel->numFrames = Header.numFrames;  // set correct number of objects (frames)

   ParseAnimations(pModel);  // create animation list and save in model struct

   for (i = 0; i < pModel->numFrames; i++)  // loop for every keyframe and save vertices
   {
      t3DObject currentFrame = {0};  // clear first frame

      currentFrame.numVerts    = Header.numVerts;  // save vertex, texcoords, and faces (tris)
      currentFrame.numTexVerts = Header.numTexVerts;
      currentFrame.numFaces    = Header.numTris;

      currentFrame.pVerts    = new CVector3 [currentFrame.numVerts];  // memory for verts, texcoords, faces

      for (j = 0; j < currentFrame.numVerts; j++)  // loop to save all vertices
      {
         currentFrame.pVerts[j].x = pFrames[i].pVerts[j].vertex[0];
         currentFrame.pVerts[j].y = pFrames[i].pVerts[j].vertex[1];
         currentFrame.pVerts[j].z = pFrames[i].pVerts[j].vertex[2];
      } // end for j

      delete pFrames[i].pVerts; // free old vertices

      if (i > 0)  // if past first keyframe, we don't need UV coords
      {
         pModel->pObject.push_back(currentFrame);  // save current frame to list
         continue;  // go to next keyframe
      } // end if

      currentFrame.pTexVerts = new CVector2[currentFrame.numTexVerts];  // memory for UV coords and face info
      currentFrame.pFaces    = new tFace[currentFrame.numFaces];

      for (j = 0; j < currentFrame.numTexVerts; j++)  // save UV coords in 0 to 1 ratio
      {
         currentFrame.pTexVerts[j].x = pTexVerts[j].u / float(Header.skinWidth);
         currentFrame.pTexVerts[j].y = 1 - pTexVerts[j].v / float(Header.skinHeight);
      } // end for j

      for (j = 0; j < currentFrame.numFaces; j++)  // save face data
      {
         currentFrame.pFaces[j].vertIndex[0] = pTris[j].vertIndex[0];
         currentFrame.pFaces[j].vertIndex[1] = pTris[j].vertIndex[1];
         currentFrame.pFaces[j].vertIndex[2] = pTris[j].vertIndex[2];

         currentFrame.pFaces[j].texIndex[0] = pTris[j].texIndex[0];
         currentFrame.pFaces[j].texIndex[1] = pTris[j].texIndex[1];
         currentFrame.pFaces[j].texIndex[2] = pTris[j].texIndex[2];
      } // end for j

      pModel->pObject.push_back(currentFrame);  // add current frame to list

   } // end for i

}


/////////////////////////////////////////////////////////////////////////////////////
// Close file and free up memory for skins, texcoords, triangles, animation frames
/////////////////////////////////////////////////////////////////////////////////////

void CLoadMD2::CleanUp()
{
   fclose(FilePointer);  // close file

   if (pSkins)    delete [] pSkins;  // free skins
   if (pTexVerts) delete pTexVerts;  // free texcoords
   if (pTris)     delete pTris;      // free triangle faces
   if (pFrames)   delete pFrames;    // free animation frames

} // end CleanUp

// END MD2.cpp
