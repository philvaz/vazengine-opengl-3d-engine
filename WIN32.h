#ifndef _WIN32_H
#define _WIN32_H

// WIN32.h by PhilVaz
// VazEngine window functions WinMain, WinProc
// Nov 20, 2005

// HEADER FILE ///////////////////////////////////////////////

#define WIN32_LEAN_AND_MEAN

#include <windows.h>
#include <windowsx.h>
#include <mmsystem.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#include <gl\gl.h>     // OpenGL32 library
#include <gl\glu.h>    // GLU32    library
#include <gl\glaux.h>  // GLAUX    library

#include "Vaz2D.h"
#include "Vaz3D.h"
#include "VazGame.h"
#include "VazInput.h"
#include "VazModel.h"
#include "VazPart.h"
#include "VazSound.h"
//#include "WIN32.h"

#include "MD2.h"
#include "VazDemo.h"

// defines for Win32

#define WINDOW_CLASS_NAME "WINCLASS1"

#define WINDOW_WIDTH   640   // size of game window
#define WINDOW_HEIGHT  480
#define WINDOW_BPP      16

#define GAME_SPEED      15   // speed of game approx 60 frames/sec (increase to slow down)

// functions for Win32

LRESULT CALLBACK WinProc(HWND hwnd, UINT msg, WPARAM wparam, LPARAM lparam);

int WINAPI WinMain(HINSTANCE hinstance, HINSTANCE hprevinstance, LPSTR lpcmdline, int ncmdshow);

#endif
