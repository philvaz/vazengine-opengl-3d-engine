#ifndef _VAZGAME_H
#define _VAZGAME_H

// VazGame.h by PhilVaz
// VazEngine general game functions GameInit, GameMain, GameQuit
// Nov 20, 2005

// HEADER FILE ///////////////////////////////////////////////

#define WIN32_LEAN_AND_MEAN

#include <windows.h>
#include <windowsx.h>
#include <mmsystem.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#include <gl\gl.h>     // OpenGL32 library
#include <gl\glu.h>    // GLU32    library
#include <gl\glaux.h>  // GLAUX    library

#include "Vaz2D.h"
#include "Vaz3D.h"
//#include "VazGame.h"
#include "VazInput.h"
#include "VazModel.h"
#include "VazPart.h"
#include "VazSound.h"
#include "WIN32.h"

#include "MD2.h"
#include "VazDemo.h"

// defines

#define GAME_STATE_DEMO_INIT   0   // demo/menu initialization state
#define GAME_STATE_DEMO_RUN    1   // demo/menu running state
#define GAME_STATE_GAME_INIT   2   // game initialization state
#define GAME_STATE_GAME_RUN    3   // game running state
#define GAME_STATE_GAME_RESET  4   // game reset state (new level)
#define GAME_STATE_GAME_PAUSE  5   // game pause state (no movement/no sound?)

// functions

void GameInit();

void GameMain();

void GameQuit();

#endif
