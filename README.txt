VazEngine 1.0

a 3D game graphics engine by PhilVaz

current features: 

OpenGL First Person view, Ground, rotating Sky-Plane, Target cross-hair, simple objects, collision, particles, basic DirectSound

future features: 

Sky-Box, 3D models, Terrain, Interiors, Select Camera and Resolution, Advanced Collision, Particle Effects and Lighting/Shading, 3D Sound

www.VazGames.com/engine

(c) 2006 
