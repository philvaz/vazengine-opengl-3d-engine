// VazSound.cpp by PhilVaz
// VazEngine sound functions
// Nov 20, 2005

#include "VazSound.h"

// globals

LPDIRECTSOUND game_sound_main = NULL;   // global direct sound object and buffers

LPDIRECTSOUNDBUFFER game_sound_explode0    = NULL;
LPDIRECTSOUNDBUFFER game_sound_explode1    = NULL;
LPDIRECTSOUNDBUFFER game_sound_explode2    = NULL;
LPDIRECTSOUNDBUFFER game_sound_explode3    = NULL;
LPDIRECTSOUNDBUFFER game_sound_explode4    = NULL;
LPDIRECTSOUNDBUFFER game_sound_explode5    = NULL;
LPDIRECTSOUNDBUFFER game_sound_explode6    = NULL;
LPDIRECTSOUNDBUFFER game_sound_explode7    = NULL;

LPDIRECTSOUNDBUFFER game_sound_blaster1    = NULL;
LPDIRECTSOUNDBUFFER game_sound_blaster2    = NULL;
LPDIRECTSOUNDBUFFER game_sound_blaster3    = NULL;
LPDIRECTSOUNDBUFFER game_sound_blaster4    = NULL;
LPDIRECTSOUNDBUFFER game_sound_blaster5    = NULL;

int player_fire = 0;       // gun sound buffers 1,2,3,4,5
int last_explode_sound;  // last sound of explosion

extern HWND game_window;

extern bool sound_ok;
extern int game_state;


///////////////////////////////////////////////////////
// Initialize DirectSound and individual sound buffers
///////////////////////////////////////////////////////

bool SoundInit()
{
   if (DirectSoundCreate(NULL, &game_sound_main, NULL) != DS_OK)
   {
      MessageBox(game_window, "Sound could not be initialized -- Direct Sound Create Error","Sound Error",MB_OK);
      return FALSE;
   }

   //
   // Direct Sound object succeeded so set coop level
   //

   if (IDirectSound_SetCooperativeLevel(game_sound_main, game_window, DSSCL_PRIORITY) != DS_OK)
   {
      MessageBox(game_window, "Sound could not be initialized -- Cooperative Level Error","Sound Error",MB_OK);
      return FALSE;
   }

   //
   // Cooperative Level succeeded so set up
   // secondary buffers and load wave file sound resources
   //
   
   game_sound_explode0    = DSLoadSoundBuffer(game_sound_main, MAKEINTRESOURCE(SOUND_EXPLODE0));
   game_sound_explode1    = DSLoadSoundBuffer(game_sound_main, MAKEINTRESOURCE(SOUND_EXPLODE1));
   game_sound_explode2    = DSLoadSoundBuffer(game_sound_main, MAKEINTRESOURCE(SOUND_EXPLODE2));
   game_sound_explode3    = DSLoadSoundBuffer(game_sound_main, MAKEINTRESOURCE(SOUND_EXPLODE3));
   game_sound_explode4    = DSLoadSoundBuffer(game_sound_main, MAKEINTRESOURCE(SOUND_EXPLODE4));
   game_sound_explode5    = DSLoadSoundBuffer(game_sound_main, MAKEINTRESOURCE(SOUND_EXPLODE5));
   game_sound_explode6    = DSLoadSoundBuffer(game_sound_main, MAKEINTRESOURCE(SOUND_EXPLODE6));
   game_sound_explode7    = DSLoadSoundBuffer(game_sound_main, MAKEINTRESOURCE(SOUND_EXPLODE7));
   
   game_sound_blaster1    = DSLoadSoundBuffer(game_sound_main, MAKEINTRESOURCE(SOUND_BLASTER1));
   game_sound_blaster2    = DSLoadSoundBuffer(game_sound_main, MAKEINTRESOURCE(SOUND_BLASTER2));
   game_sound_blaster3    = DSLoadSoundBuffer(game_sound_main, MAKEINTRESOURCE(SOUND_BLASTER3));
   game_sound_blaster4    = DSLoadSoundBuffer(game_sound_main, MAKEINTRESOURCE(SOUND_BLASTER4));
   game_sound_blaster5    = DSLoadSoundBuffer(game_sound_main, MAKEINTRESOURCE(SOUND_BLASTER5));
   
   // return TRUE since main DirectSound initialized ok

   return TRUE;

} // END OF SoundInit


//////////////////////////////////////////////////////
// Stop all sounds and close DirectSound and buffers
/////////////////////////////////////////////////////

void SoundQuit()
{
   // if sound was set okay, release DirectSound objects
   if (game_sound_main)
   {
      // FIRST RELEASE SECONDARY BUFFERS

      IDirectSound_Release(game_sound_blaster5);
      IDirectSound_Release(game_sound_blaster4);
      IDirectSound_Release(game_sound_blaster3);
      IDirectSound_Release(game_sound_blaster2);
      IDirectSound_Release(game_sound_blaster1);
      
      IDirectSound_Release(game_sound_explode7);
      IDirectSound_Release(game_sound_explode6);
      IDirectSound_Release(game_sound_explode5);
      IDirectSound_Release(game_sound_explode4);
      IDirectSound_Release(game_sound_explode3);
      IDirectSound_Release(game_sound_explode2);
      IDirectSound_Release(game_sound_explode1);
      IDirectSound_Release(game_sound_explode0);

      // THEN RELEASE MAIN DIRECT SOUND OBJECT
      IDirectSound_Release(game_sound_main);
   }

} // END OF SoundQuit


//////////////////////////////////////////////////////////////
// Plays occasional random sound for background noise
//////////////////////////////////////////////////////////////

void PlayRandomSound()
{

   if (!sound_ok || game_state != GAME_STATE_GAME_RUN) return;

   
     
} // END OF PlayRandomSound


//////////////////////////////////////////
// Play Random Explosion noise
//////////////////////////////////////////

void PlayRandomExplosion(int v)
{
   int s;
   
   if (!sound_ok || game_state != GAME_STATE_GAME_RUN) return;
   
   // type sound or volume v
                  
   if (v == 0)
   {
      do
      {
         s = (rand()%4);
      }   
      while (last_explode_sound == s);
      
      if (s == 0) IDirectSoundBuffer_Play(game_sound_explode1,0,0,NULL);
      if (s == 1) IDirectSoundBuffer_Play(game_sound_explode2,0,0,NULL);
      if (s == 2) IDirectSoundBuffer_Play(game_sound_explode3,0,0,NULL);
      if (s == 3) IDirectSoundBuffer_Play(game_sound_explode4,0,0,NULL);
      last_explode_sound = s;
   }
   
   if (v == 1)
   {
      do
      {
         s = (rand()%7);
      }
      while (last_explode_sound == s);

      if (s == 0) IDirectSoundBuffer_Play(game_sound_explode1,0,0,NULL);
      if (s == 1) IDirectSoundBuffer_Play(game_sound_explode2,0,0,NULL);
      if (s == 2) IDirectSoundBuffer_Play(game_sound_explode3,0,0,NULL);
      if (s == 3) IDirectSoundBuffer_Play(game_sound_explode4,0,0,NULL);
      if (s == 4) IDirectSoundBuffer_Play(game_sound_explode5,0,0,NULL);
      if (s == 5) IDirectSoundBuffer_Play(game_sound_explode6,0,0,NULL);
      if (s == 6) IDirectSoundBuffer_Play(game_sound_explode7,0,0,NULL);
      last_explode_sound = s;
   }
   
} // END OF PlayRandomExplosion

